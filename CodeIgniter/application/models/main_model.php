<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function get_notequal($id,$table)
    {
       $this->db->where('id !=',$id);
        $query=$this->db->get($table);
        return $query->result_array();
    }
    ///////// Users /////////////
    public function add_user($data)
    {
        $this->db->insert('Users',$data);
    }
    public function get_user($id)
    {
        $query = $this->db->get_where('Users' ,array('id' => $id));
        return $query->result_array();
    }
    public function delete_user($id)
    {
        $this->db->delete('Users', array('id' => $id)); 
    }
    public function update_user($id,$data)
    {
        $this->db->where('id', $id);
        $this->db->update('Users', $data);
    }
    public function list_user()
    {
        $this->db->where('role', 'user');
        $query = $this->db->get('Users');
        return $query->result_array();
    }
    public function check_mail($mail)
    {
         $query = $this->db->get_where('Users', array('email' => $mail), 1);
         return $query;
    }
    public function login_user($email,$password)
    {
         $query = $this->db->get_where('Users', array('password' => $password ,'email' => $email ));
         return $query;
    }
    public function update_reset($mail,$data)
    {
        $this->db->set('resetpassword',$data);
        $this->db->where('email', $mail);
        $this->db->update('Users');
    }
    public function confirm_reset($code)
    {
        $query = $this->db->get_where('Users', array('resetpassword' => $code ));
        return $query;
    }
    public function update_password($rest,$data)
    {
        $this->db->set('password', $data);
        $this->db->set('resetpassword', '');
        $this->db->where('resetpassword', $rest);
        $this->db->update('Users');
    }
    public function get_user_bydate()
    {
        $this->db->order_by('month(date)', "asc"); 
        $this->db->group_by('month(date)');
        $this->db->select('count(*) as num');
        $query=  $this->db->get('Users');
        return $query->result_array();
    }
    ///////// Menus ////////////
   public function add_menu_item($data)
   {
       $this->db->insert('Menus',$data);
   }
   public function update_menu_item($id,$data)
   {
       $this->db->where('id',$id);
       $this->db->update('Menus',$data);
   }
   public function delete_menu_item($id)
   {
       $this->db->delete('Menus', array('id' => $id)); 
   }
   public function get_menu_item($id)
   {
       $query=$this->db->get_where('Menus', array('id' => $id)); 
       return $query->result_array();
   }
   public function list_menu_items()
   {
       $query = $this->db->get('Menus');
       return $query->result_array();
   }
   public function get_menu_items($type)
   {
       $query = $this->db->get_where('Menus',array('type' => $type ));
       return $query->result_array();
   }
   ///////// Pages ////////////
   public function add_page($data)
   {
       $this->db->insert('Pages',$data);
   }
   public function update_page($id,$data)
   {
       $this->db->where('id',$id);
       $this->db->update('Pages',$data);
   }
   public function delete_page($id)
   {
       
       $this->db->select('name');
       $this->db->where('id',$id);
       $urls=$this->db->get('Pages');
       $urls=$urls->result_array();
       $name=$urls[0]['name'];
       $this->db->delete('Menus',array('url'=>$name));
       $this->db->delete('Pages', array('id' => $id)); 
   }
   public function get_page($name)
   {
       $query = $this->db->get_where('Pages',array('name' => $name ));
       return $query->result_array();
   }
   public function get_page_by_id($id)
   {
       
       $query = $this->db->get_where('Pages',array('id' => $id ));
       return $query->result_array();
   }

   public function list_pages()
   {
       $query = $this->db->get('Pages');
       return $query->result_array();
   }
   public function get_menu_pages()
   {
      $menu=$this->get_all_menupages(); 
      $url=array();
      foreach ($menu as $menu_page)
      {
          $url[$menu_page['url']]=$menu_page['url'];
      }
      $this->db->where_not_in('name',$url);
      $pages=$this->db->get('Pages');
      return $pages->result_array();
   }
   public function get_all_menupages()
   {
      $this->db->select('url');
      $menu_pages=$this->db->get('Menus');
      return $menu_pages->result_array();
      
   }
   ///////////// Slider //////////////
   public function add_slider($data)
   {
       $this->db->insert('Images',$data);
   }
   public function delete_slider($id)
   {
       $this->db->delete('Images', array('id' => $id)); 
   }
   public function get_slider()
   {
       $query=$this->db->get('Images');
       return $query->result_array();
   }
   public function get_image($id)
   {
       $query=$this->db->get_where('Images',array('id'=>$id));
       return $query->result_array();
   }
   public function update_image($id,$data)
   {
       $this->db->where('id',$id);
       $this->db->update('Images',$data);
   }
   //////////// social ////////////////////
   public function list_social($status)
   {
       $query=  $this->db->get_where('Social',array('status'=>$status));
       return $query->result_array();
   }
   public function edit_social($id,$data)
   {
       $this->db->where('id',$id);
       $this->db->update('Social',$data);
   }
   public function get_social($id)
   {
       $query=$this->db->get_where('Social',array('id'=>$id));
       return $query->result_array();
   }
   public function list_all_social()
   {
       $query=$this->db->get('Social');
       return $query->result_array();
   }
   ////////////// Tools //////////////
    public function add_tool($data)
   {
       $this->db->insert('Tools',$data);
   }
   
   public function list_tools()
   {
       $query = $this->db->get('Tools');
       return $query->result_array();
   }
  public function delete_tool($id)
   {
       $this->db->delete('Tools', array('id' => $id)); 
   } 
  public function get_tool($id)
   {
        $query = $this->db->get_where('Tools' ,array('id' => $id));
       return $query->result_array();
   }   
  public function update_tool($id,$data)
    {
        $this->db->where('id', $id);
        $this->db->update('Tools', $data);
    }
    ////////////// Packages /////////////////////
  public function add_package($data,$tools)
    {
      $this->db->insert('Packages',$data);
      $this->db->select_max('id');
      $package=$this->db->get('Packages');
      $package=$package->result_array();
      foreach ($tools as $tool)
      {
          $data=array(
                        'package_id'=>$package[0]['id'],
                        'tool_id'=>$tool
                    );
          $this->db->insert('Packeges_tools',$data);
      }
    }
  public function update_package($id,$data,$tools)
    {
      $this->db->where('package_id',$id);
      $this->db->delete('Packeges_tools');
      $this->db->where('id',$id);
      $this->db->update('Packages',$data);
      foreach ($tools as $tool)
      {
          $data=array(
                        'package_id'=>$id,
                        'tool_id'=>$tool
                    );
          $this->db->insert('Packeges_tools',$data);
      }
    }
  public function delete_package($id)
    {
        $this->db->where('package_id',$id);
        $this->db->delete('Packeges_tools');
        $this->db->where('id',$id);
        $this->db->delete('Packages');
    }
  public function get_package($id)
    {
        $query=$this->db->get_where('Packages',array('id'=>$id));
        return $query->result_array();
    }
  public function get_pakage_tools($id)
    {
        $query= $this->db->get_where('Packeges_tools',array('package_id'=>$id));
        return $query->result_array();
    }
  public function list_packages()
    {
        $query=$this->db->get('Packages');
        return $query->result_array();
    }
     ////////////// News /////////////////////
  public function add_news($data)
    {
      $this->db->insert('News',$data);
    }
    public function update_news($id,$data)
    {
      
      $this->db->where('id',$id);
      $this->db->update('News',$data);
    
     
    }
     public function list_news()
    {
        $query=$this->db->get('News');
        return $query->result_array();
    }
     public function get_news($id)
    {
        $query=$this->db->get_where('News',array('id'=>$id));
        return $query->result_array();
    }
      public function delete_news($id)
    {
       
        $this->db->where('id',$id);
        $this->db->delete('News');
    }
}


