<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
    public $editor = array();
    public function __construct()
	{
            parent::__construct();
            $this->load->helper(array('form', 'url'));
            $this->load->library('session');
            $this->load->helper('ckeditor');
            $this->load->model('main_model');
            $this->load->library('upload');
            $this->load->helper('date');
            $this->load->library('javascript');
            $this->load->library('email'); 
            $this->editor['ckeditor'] = array(
			//ID of the textarea that will be replaced
			'id' 	=> 	'details',
			'path'	=>	base_url().'resources/js/ckeditor',
			//Optionnal values
			'config' => array(
				'toolbar' => "Full", 	//Using the Full toolbar
				'width' => "550px",	//Setting a custom width
				'height'=> '100px',	//Setting a custom height	
			),
			//Replacing styles from the "Styles tool"
			'styles' => array(
			
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 	=> 	'Blue',
						'font-weight' 	=> 	'bold'
					)
				)));
            $this->session->set_flashdata('error_message', "");
	}
        public function index()
	{
            $slider['images']=$this->main_model->get_slider();
            $slider['packages']=$this->main_model->list_packages();
            $this->my_view('home',$slider);
	}
        public function home()
	{
            redirect('main/index');
	}
       
        public function admin_board()
        {
            if($this->session->userdata('role')=='admin')
            {
                $this->load->view('admin_view/admin_menu');
            }
            else
            {
               $this->load->view('invalid');
            }
             
        }

        public function my_view($page,$data="",$editor="",$error="")
	{
            $header['items']=  $this->main_model->get_menu_items('header');
            $footer['items']=  $this->main_model->list_social('show');
            $this->load->view('layout/header',$header);
            $this->load->view($page,$data,$editor,$error);
            $this->load->view('layout/footer',$footer);
            
	}

    ////////////// User /////////////////
    public function register()
	{
            $this->load->library('form_validation');
            // field name, error message, validation rules
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[Users.email]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('confirmpassword', 'Password Confirmation', 'trim|required|matches[password]');
            if($this->form_validation->run())
            {
                
                $data=array(
                       'name'=>$this->input->post('name'),
                       'email'=>$this->input->post('email'),
                       'password'=>md5($this->input->post('password')),
                       'date'=> date('Y-m-d H:i:s',now()),
                       );   
                $this->main_model->add_user($data);
                redirect('main/index');
              }
              else
               {
                 
                  $this->my_view('user_view/registeration');          
               }
           }
           public function edit_user()
           {
               if($this->session->userdata('id')!='') 
               {
                    $this->load->library('form_validation');
                    // field name, error message, validation rules
                    $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
                    $this->form_validation->set_rules('job','job', 'trim|xss_clean');
                    $this->form_validation->set_rules('image','image','callback_image_upload');
                    if($this->form_validation->run())
                    {

                        $data= $this->upload->data();
                        if($data['file_name']== '')
                        {
                            $data=array(
                                        'name'=>$this->input->post('name'),
                                        'image'=>$this->session->userdata('image'),
                                        'job'=>$this->input->post('job')
                                       );     
                        }
                        else 
                        {
                            $image= $data['file_name'];
                            $name=$this->input->post('name');
                            $this->session->set_userdata('username', $name);
                            $this->session->set_userdata('image', $image);
                            $data=array(
                                        'name'=>$name,
                                        'image'=>$image,
                                       ); 
                        } 
                        $id=  $this->session->userdata('id');
                        $this->main_model->update_user($id,$data);
                        redirect('main/show_profile');
                    }
                    else
                    {
                        $id=  $this->session->userdata('id');
                        $data['user']=  $this->main_model->get_user($id);   
                        $this->my_view('user_view/profile',$data);       
                    }
                }
                else 
                {
                    redirect('main/login');
                }
        }
        public function image_upload()
        {
	  if($_FILES['image']['size'] != 0)
              {
                
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = 1000;
                $config['file_name']= 'image_'.substr(md5(rand()),0,7);
                $this->upload->initialize($config);
		if (!$this->upload->do_upload('image'))
                {
                    $this->form_validation->set_message('image_upload', $this->upload->display_errors());
                    return  FALSE;
                }	
		else
                {
                    return  TRUE;
		}	
            }
           else
               
            {
                return  0;
            } 
        }
        public function list_user()
        {
            if($this->session->userdata('role')=='admin')
            {
                $data['users']= $this->main_model->list_user();
                $this->admin_board();
                $this->load->view('user_view/list_user',$data);
            }
            else
            {
                $this->load->view('invalid');
            }
        }
        public function login()
        {
            if($this->session->userdata('id')== '')
            {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
                $error['user']="";
                if($this->form_validation->run())
                {
                    $email= $this->input->post('email');
                    $password=md5($this->input->post('password'));
                    $user_info= $this->main_model->login_user($email,$password);
                    $user=$user_info->result();
                    if($user_info->num_rows > 0)
                    {
                        $user_data=array('id'=> $user[0]->id,
                                         'username'=> $user[0]->name,
                                         'useremail'=> $user[0]->email,
                                         'role'=> $user[0]->role,
                                         'image'=> $user[0]->image,
                                        );
                        $this->session->set_userdata($user_data);
                        redirect('main/show_profile');   
                    }
                    else
                    {
                        $error['user']="Invalid username or password";
                        
                        $this->my_view('user_view/login',$error);
                    }
                }
                else 
                {
                    //$this->load->view('user_view/login',$error);
                    
                    $this->my_view('user_view/login',$error);
                }
            }
            else 
            {
                redirect('main/show_profile');
            }
        }
        public function show_profile()
        { 
            $id=  $this->session->userdata('id');
            $data['user']=  $this->main_model->get_user($id);
            $this->my_view('user_view/profile',$data);
        }
        public function logout()
        {
            $newdata = array('id'=>'',
                             'username'=>'',
                             'useremail'=> '',
                             'role'=>'',
                             'image'=>''
                            );
            $this->session->unset_userdata($newdata );
            $this->session->sess_destroy();
            redirect('main/index');
        }
        public function forgetpassword()
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_exists');
            if($this->form_validation->run())
            {
                $config = array('protocol' => 'smtp',
                                'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port'=>465,
                                'smtp_user' => 'wafiamustafa@gmail.com',
                                'smtp_pass' => 'w1a2f3i4a5',
                                'mailtype' => 'html',
                                'charset' => 'iso-8859-1',
                                'wordwrap' => TRUE
                               );
                $this->email->initialize($config);
                $email = $this->input->post('email');
		$code = md5(rand(0,9).$email);
                $this->email->set_newline("\r\n");
		$this->email->from('wafiamustafa@gmail.com', 'SDLC_Admin'); 
		$this->email->to($email); 
		$this->email->subject('Reset your Password');
                $this->email->message(
                                    "<p> To reset your password please click the link below and follow the instructions:
                                    http://localhost/CodeIgniter/main/resetpassword/".$code."</p>"); 
                if ( ! $this->email->send())
                {
                    //  error
                }
                $this->main_model->update_reset($email,$code);
            }
           
               $this->my_view('user_view/forgetpassword');
        }
        public function email_exists($email)
	{	
            $data=$this->main_model->check_mail($email); 
            if($data->num_rows > 0)
            {
                return true;
            } 
            else 
            {
                $this->form_validation->set_message('email_exists', 'This email address does not found in our system.');
                return false;
            }
	}
        public function resetpassword($code)
        {
            $data=$this->main_model->confirm_reset($code);
            if($data->num_rows > 0)
            {
                $user=$data->result_array();
                $email=$user[0]['email'];
                $this->load->library('form_validation');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
                $this->form_validation->set_rules('confirmpassword', 'Password Confirmation', 'trim|required|matches[password]');
                if($this->form_validation->run())
                {
                    $password=md5($this->input->post('password'));
                    $this->main_model->update_password($code,$password); 
                    redirect('main/login');
                }
                $reset['code']=$code;
                $this->my_view('user_view/resetpassword',$reset);
            } 
            else
            {
                $this->my_view('invalid');
            }
        }
        public function delete_user($id)
        {
            if($this->session->userdata('role')=='admin')
            {
                $this->main_model->delete_user($id);
                redirect('main/list_user');
            }
            else
            {
                $this->load->view('invalid');
            }
        }
        public function user_by_date()
        {
            if($this->session->userdata('role')=='admin')
            {
                $data['users']= $this->main_model->get_user_bydate();
                $num=array();
                $i=0;
                foreach($data['users'] as $user)
                {
                    $num[$i]= (int)$user['num']; 
                    $i++;
                }
                $series_data[]=array('name'=>"Users",'data'=>$num);
                $this->view_data['series_data']=json_encode($series_data);
                $this->admin_board();
                $this->load->view('admin_view/pages/reports/user_report',$this->view_data);
            }
            else
            {
                $this->load->view('invalid');
            }
        }

        ///////////// menu ///////////////
     public function add_menu_item()
     {
         if($this->session->userdata('role')== "admin")
         {
            $this->load->library('form_validation');
            // field name, error message, validation rules
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean|is_unique[Menus.name]|alpha_dash');
            $this->form_validation->set_rules('url', 'url', 'trim|required|xss_clean|is_unique[Menus.url]');
            if($this->form_validation->run())
            {
                if($this->input->post('position')== 0)
                {
                    $type="header";
                }
                else
                {
                    $type="footer";
                } 
                $data=array(
                       'name'=>$this->input->post('name'),
                       'url'=>$this->input->post('url'),
                       'type'=>$type,           
                       );   
                $this->main_model->add_menu_item($data);
                redirect('main/list_menu_item');
            }
            else
            {
                $pages['pages']= $this->main_model->get_menu_pages();
                if(empty($pages['pages']))
                {
                    $this->admin_board();
                    $this->load->view('admin_view/pages/menus/message');
                }
                else 
                {
                    $this->admin_board();
                    $this->load->view('admin_view/pages/menus/add_menu_item',$pages);
                }
            }

         }
        else 
         {
            $this->load->view('invalid');
         }
     }
     public function edit_menu_item($id)
     {
         $item_data['items']=  $this->main_model->get_menu_item($id);
         if($this->session->userdata('role')== "admin")
         {
            $this->load->library('form_validation');
            // field name, error message, validation rules
                if($this->input->post('position')== 0)
                {
                    $type="header";
                }
                else
                {
                    $type="footer";
                } 
            if($this->form_validation->run())
            {       
                $data=array(
                       'type'=>$type,           
                       );   
                $this->main_model->update_menu_item($id,$data);
                redirect('main/list_menu_item');
            }
            else
            {
                $this->admin_board();
                $this->load->view('admin_view/pages/menus/edit_menu_item',$item_data);
            }
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function delete_menu_item($id)
     {
         if($this->session->userdata('role')== "admin")
         {
              $this->main_model->delete_menu_item($id);
              redirect('main/list_menu_item');
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function list_menu_item()
     {
         if($this->session->userdata('role')== "admin")
         {
             $data['items']= $this->main_model->list_menu_items();
             $this->admin_board();
             $this->load->view('admin_view/pages/menus/header',$data);
         }
         else
         {
             $this->load->view('invalid');
         }
     }
  
      public function add_page()
     {
         if($this->session->userdata('role')== "admin")
         {
            $this->load->library('form_validation');
            // field name, error message, validation rules
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean|is_unique[Pages.name]|alpha_dash');
            $this->form_validation->set_rules('details', 'details', 'trim|required|xss_clean|is_unique[Pages.details]');
            if($this->form_validation->run())
            {
                $data=array(
                       'name'=>$this->input->post('name'),
                       'details'=>$this->input->post('details'),          
                       );   
                $this->main_model->add_page($data);
                redirect('main/list_pages');
            }
            else
            {
                $this->admin_board();
                $this->load->view('admin_view/pages/pages/add_page', $this->editor);
            }

         }
         else
         {
             $this->load->view('invalid');
         }
     }
    
     public function delete_page($id)
     {
         if($this->session->userdata('role')== "admin")
         {
            $this->main_model->delete_page($id);
            redirect('main/list_pages');
         }
         else
         {
            $this->load->view('invalid');
         }
     }
     public function list_pages()
     {
         if($this->session->userdata('role')== "admin")
         {
             $data['pages']= $this->main_model->list_pages();
             $this->admin_board();
             $this->load->view('admin_view/pages/pages/list_pages',$data);
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function edit_page($id)
     {
         
         if($this->session->userdata('role')== "admin")
         {
            $page_data['pages']=  $this->main_model->get_page_by_id($id); 
            $this->load->library('form_validation');
            // field name, error message, validation rules
            $this->form_validation->set_rules('details', 'detail', 'trim|required|xss_clean');
            
            if($this->form_validation->run())
            {       
                $data=array(
                       'details'=>$this->input->post('details'),         
                       );   
                $this->main_model->update_page($id,$data);
                redirect('main/list_pages');
            }
            else
            {
                
                $this->admin_board();
                $this->load->view('admin_view/pages/pages/edit_page',$page_data,$this->editor);
            }
         }
         else
         {
             $this->load->view('invalid');
         }   
     }
     public function page($name)
     {
        if($name=='Home')
        {
            redirect('main/index');
        }
        else if($name=='Contact')
        {
            redirect('main/contact');
        }
        else
        {
            $data['content']= $this->main_model->get_page($name);
            $this->my_view('pages/page_view',$data);
        }
     }
     ///////////// Slider ////////////////
     public function add_image()
     {
         if($this->session->userdata('role')== "admin")
         {
            $this->load->library('form_validation');
            // field name, error message, validation rules
            $this->form_validation->set_rules('image', 'image','callback_image_upload');
            $this->form_validation->set_rules('details', 'details', 'trim|required|xss_clean');
            $var['error']="";
            if($this->form_validation->run())
            {
                $data= $this->upload->data();
                if($data['file_name']== '')
                {
                   $var['error']="image required"; 
                   $this->load->view('admin_view/pages/imagesadd_slider',$var);
                }
                else
                {
                    $image= $data['file_name'];
                    $data=array(
                           'image'=>$image,
                           'details'=>$this->input->post('details'),          
                           );   
                    $this->main_model->add_slider($data);
                    redirect('main/list_images');
                }
            }
            else
            {
                $this->admin_board();
                $this->load->view('admin_view/pages/images/add_slider',$var, $this->editor);
            }

         }
         else
         {
             $this->load->view('invalid');
         }

     }
     public function edit_image($id)
     {
         if($this->session->userdata('role')=='admin')
         {
            $image_data['images']= $this->main_model->get_image($id);
            $this->load->library('form_validation');
            // field name, error message, validation rules
            $this->form_validation->set_rules('details', 'detail', 'trim|required|xss_clean');
            
            if($this->form_validation->run())
            {       
                $data=array(
                            'details'=>$this->input->post('details'),
                            );   
                $this->main_model->update_image($id,$data);
                redirect('main/list_images');
            }
            else
            {
                
            }
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function list_images()
     {
         if($this->session->userdata('role')=='admin')
         {
             $data['images']= $this->main_model->get_slider();
             $this->admin_board();
             $this->load->view('admin_view/pages/images/list_images',$data);
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function delete_image($id)
     {
         if($this->session->userdata('role')=='admin')
         {
             $this->main_model->delete_slider($id);
             redirect('main/list_images');
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     ///////// Social //////////////////
     public function list_social()
     {
         if($this->session->userdata('role')=='admin')
         {
            $data['social']=  $this->main_model->list_all_social();
            $this->admin_board();
            $this->load->view('admin_view/pages/menus/footer',$data);
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function edit_social($id)
     {
         $item_data['items']=  $this->main_model->get_social($id);
         if($this->session->userdata('role')== "admin")
         {
            $this->load->library('form_validation');
            // field name, error message, validation rules
                 $this->form_validation->set_rules('url', 'url', 'trim|required|xss_clean|valid_url');
                if($this->input->post('status')== 0)
                {
                    $status="show";
                }
                else
                {
                    $status="hide";
                } 
            if($this->form_validation->run())
            {       
                $data=array(
                        'url'=>$this->input->post('url'),
                        'status'=>$status,           
                       );   
                $this->main_model->edit_social($id,$data);
                redirect('main/list_social');
            }
            else
            {
                $this->admin_board();
                $this->load->view('admin_view/pages/menus/edit_social',$item_data);
            }
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     /////////// Contact ///////////////
     public function contact()
     {
         $this->my_view('contact');
     }
     ////////////// Packages ////////////
     public function add_package()
     {
         if($this->session->userdata('role')== 'admin')
         {
             $this->load->library('form_validation');
             // field name, error message, validation rules
             $this->form_validation->set_rules('package_name', 'name', 'trim|required|xss_clean');
             $this->form_validation->set_rules('users_num', 'num_users', 'trim|required|xss_clean|integer');
             $this->form_validation->set_rules('projects_num', 'num_packages', 'trim|required|xss_clean|integer');
             if($this->form_validation->run())
             {
                $data=array(
                        'name'=>$this->input->post('package_name'),
                        'project_num'=>$this->input->post('projects_num'),
                        'users_num'=>$this->input->post('users_num'), 
                        'price'=>$this->input->post('price'),          
                     );
                $tools=$this->input->post('check');
                $package=$this->main_model->add_package($data,$tools);
                redirect('main/list_packages');
             }
             else
             {
                $data['tools']= $this->main_model->list_tools(); 
                $this->admin_board();
                $this->load->view('admin_view/pages/packages/add_package',$data); 
             }
             
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function edit_package($id)
     {
         if($this->session->userdata('role')== 'admin')
         {
            $this->load->library('form_validation');
             // field name, error message, validation rules
            $this->form_validation->set_rules('package_name', 'name', 'trim|required|xss_clean|alpha_dash');
            $this->form_validation->set_rules('users_num', 'num_users', 'trim|required|xss_clean|integer');
            $this->form_validation->set_rules('projects_num', 'num_packages', 'trim|required|xss_clean|integer'); 
            if($this->form_validation->run())
            {
               $data=array(
                        'name'=>$this->input->post('package_name'),
                        'project_num'=>$this->input->post('projects_num'),
                        'users_num'=>$this->input->post('users_num'), 
                        'price'=>$this->input->post('price'),          
                     );
                $tools=$this->input->post('check');
                $package=$this->main_model->update_package($id,$data,$tools);
                redirect('main/list_packages');
            }
            else
            {
                
               $data['packages']=$this->main_model->get_package($id);
               $data['tools']=$this->main_model->list_tools();
               $this->admin_board();
               $this->load->view('admin_view/pages/packages/edit_package',$data);
            }
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function list_packages()
     {
         if($this->session->userdata('role')== 'admin')
         {
             $data['packages']=$this->main_model->list_packages();
             $this->admin_board();
             $this->load->view('admin_view/pages/packages/list_packages',$data);
         }
         else
         {
             $this->load->view('invalid');
         }
          
     }
     public function delete_package($id)
     {
         if($this->session->userdata('role')== 'admin')
         {
             $this->main_model->delete_package($id);
             redirect('main/list_packages');
         }
         else
         {
            $this->load->view('invalid');
         }
     }
     ////////////// Tools ////////////
     public function add_tool()
     {
         if($this->session->userdata('role')== 'admin')
         {
             $this->load->library('form_validation');
            // field name, error message, validation rules
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean|is_unique[Tools.name]|alpha_dash');
            $this->form_validation->set_rules('description', 'description', 'trim|required|xss_clean|is_unique[Tools.description]');
           $this->form_validation->set_rules('price', 'price_rule', 'trim|required|xss_clean|decimal');
            if($this->form_validation->run())
            {
                $data=array(
                       'name'=>$this->input->post('name'),
                       'description'=>$this->input->post('description'), 
                        'price'=>$this->input->post('price'),
                       );   
                $this->main_model->add_tool($data);
                redirect('main/list_tools');
            }
            else
            {
                $this->admin_board();
                $this->load->view('admin_view/pages/tools/add_tool', $this->editor);

            }

         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function edit_tool($id)
     {
         if($this->session->userdata('role')== 'admin')
         {
            $data['tools']=$this->main_model->get_tool($id);
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean'); 
            $this->form_validation->set_rules('description', 'description', 'trim|required|xss_clean'); 
            $this->form_validation->set_rules('price', 'price', 'trim|required|xss_clean|decimal'); 

            if($this->form_validation->run())
              {       
                  $data=array(
                         'name'=>$this->input->post('name'),
                         'description'=>$this->input->post('description'),        
                          'price'=>$this->input->post('price'),        
                         );   
                         //echo $id;
                  $this->main_model->update_tool($id,$data);
                   redirect('main/list_tools');

              }
            else
            {
               $this->admin_board();
               $this->load->view('admin_view/pages/tools/edit_tool',$data);
            }
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function list_tools()
     {
         
         if($this->session->userdata('role')== "admin")
         {
             $data['tools']= $this->main_model->list_tools();
             $this->admin_board();
             $this->load->view('admin_view/pages/tools/list_tools',$data);
         }
         else
         {
             $this->load->view('invalid');
         }
     }
     public function delete_tool($id)
        {
            if($this->session->userdata('role')=='admin')
            {
                $this->main_model->delete_tool($id);
                redirect('main/list_tools');
            }
            else
            {
                $this->load->view('invalid');
            }
        }
         ////////////////// News /////////////////////

        public function add_news(){
        if($this->session->userdata('role')== 'admin')
        {
            $this->load->library('form_validation');
             // field name, error message, validation rules
            $this->form_validation->set_rules('author', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('summery', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('details', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('date', 'name', 'trim|required|xss_clean');
            
             if($this->form_validation->run())
             {
                $data=array(
                        'author'=>$this->input->post('author'),
                        'summery'=>$this->input->post('summery'),
                        'details'=>$this->input->post('details'), 
                        'date'=>$this->input->post('date'),          
                    );
                $this->main_model->add_news($data);
                 redirect('main/list_news');
            }
            else
            {
                $this->admin_board();
                $this->load->view('admin_view/pages/news/add_news',$this->editor); 
               
            }
             
        }
        else
        {
            $this->load->view('invalid');
        }
    }

    public function edit_news($id)
     {

         if($this->session->userdata('role')== 'admin')
         {
            
            $data['news']=$this->main_model->get_news($id);
            $this->load->library('form_validation');

            $this->form_validation->set_rules('author', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('summery', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('details', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('date', 'name', 'trim|required|xss_clean');
            if($this->form_validation->run())
              {       
                  $data=array (
                        'author'=>$this->input->post('author'),
                        'summery'=>$this->input->post('summery'),
                        'details'=>$this->input->post('details'), 
                        'date'=>$this->input->post('date'),          
                    );       
                           
                        
                  $this->main_model->update_news($id,$data);
                  redirect('main/list_news');
                   

              }
            else
            {
               $this->admin_board();
               $this->load->view('admin_view/pages/news/edit_news',$data);
            }
         }
         else
         {
             
             $this->load->view('invalid');
         }
     }
      public function list_news()
     {
         if($this->session->userdata('role')== 'admin')
         {
             $data['news']=$this->main_model->list_news();
             $this->admin_board();
             $this->load->view('admin_view/pages/news/list_news',$data);
         }
         else
         {
             $this->load->view('invalid');
         }
          
     }
      public function delete_news($id)
        {
            if($this->session->userdata('role')=='admin')
            {
                $this->main_model->delete_news($id);
                redirect('main/list_news');
            }
            else
            {
                $this->load->view('invalid');
            }
        }
}
   
