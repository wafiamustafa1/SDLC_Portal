<html>
<head lang="en">
<meta charset="UTF-8">
<title>SDLC </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<?php 
  $css1=base_url().'resources/css/bootstrap.min.css';
  $css2=base_url().'resources/css/bootstrap-theme.min.css';
  $css3=base_url().'resources/css/mycss.css';
  $font1=base_url().'resources/fonts/glyphicons-halflings-regular.svg';
  $font2=base_url().'resources/font-awesome/css/font-awesome.min.css';
  
?>
 <link rel="stylesheet" type="text/css" href="<?php echo $css1 ?>">
 <link rel="stylesheet" type="text/css" href="<?php echo $css2 ?>">
<link rel="stylesheet" type="text/css" href="<?php echo $css3 ?>">
<link rel="stylesheet"  href="<?php echo $font1 ?>">
<link rel="stylesheet"  href="<?php echo $font2 ?>">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>


<?php 
  
   $font2=base_url().'resources/font-awesome/css/font-awesome.min.css';
   $js1=base_url().'resources/js/jquery.min.js';
   $js3=base_url().'resources/js/myjs.js';
   $js2=base_url().'resources/js/bootstrap.min.js';
   
   $js4=base_url().'resources/js/html5shiv.js';
   $js5=base_url().'resources/js/jquery-1.10.2.min.js';
   $js6=base_url().'resources/js/jquery-migrate-1.2.1.min.js';
   $js7=base_url().'resources/js/jquery.easing.1.3.js';
  $js9=base_url().'resources/js/script.js';
   

   
   
   
   
   
   
   ?>
   <script src="<?php echo $js1 ?>"></script>
   <script src="<?php echo $js2 ?>"></script>
   <script src="<?php echo $js3 ?>"></script>
   
   <script src="<?php echo $js4 ?>"></script>
   <script src="<?php echo $js5 ?>"></script>
   <script src="<?php echo $js6 ?>"></script>
   <script src="<?php echo $js7 ?>"></script>
   <script src="<?php echo $js8 ?>"></script>
   <script src="<?php echo $js9 ?>"></script>
   

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!--<link rel="stylesheet" href="/resources/demos/style.css">-->

  <script>
  $(function() {
    $( "#datepicker" ).datepicker({
      showOn: "button",
      buttonImage: "<?php $logos=base_url().'resources/images/calendar.gif'; echo $logos ;?>",
      buttonImageOnly: true,
      buttonText: "Select date"
    });
  });
  </script>

</head>
<body>

<div class="container-fluid">
<DIV CLASS="ROW">
        <div class="navbar-header  ">
             <div class="logo col-md-6 col-xs-6 col-sm-6">
                 <!--<a class="navbar-brand" href="index.html"><img src="<?php $logo=base_url().'resources/images/4.png'; echo $logo ;?>"  /></a>-->

             </div>
            </DIV>  
     
    <div class="pull-right col-md-6 text-right col-xs-6 col-sm-6" style="margin-top:80px; margin-right: 20px;" >
               <?php if($this->session->userdata('role') == ''){ ?>
                    <a href="<?php  $login_url=base_url().'main/login';echo $login_url;?>" >Sign in </a>| <a href="<?php  $login_url=base_url().'main/login';echo $login_url;?>">Sign up</a>
                <?php } 
                else { $profile_url=base_url().'main/show_profile'; ?>
                    <a href="<?php echo $profile_url;?>">
                        <?php
                         echo 'welcome, '.$this->session->userdata('username'); 
                         ?>
                         </a>
               
                    |<a href="<?php $logout=base_url().'main/logout'; echo $logout;?>">
                            logout
                    </a>
                       <?php }?>
            </div>
            
    </div>
       
  
    <div class="row">
    
<!--    <nav class=" col-md-6 col-md-offset-3  col-xs-12 col-sm-12 ">


      <div class="menu4">
          <a class="scroll"  href="<?php  $home_url=base_url().'main/home';echo $home_url;?>" ><span>Home</span></a>
        <a href="<?php  $news_url=base_url().'main/news';echo $news_url;?>"><span>News</span></a>
        <a href="<?php  $events_url=base_url().'main/events';echo $events_url;?>"><span>Events</span></a>
        <a href="<?php  $contact_url=base_url().'main/contact';echo $contact_url;?>"><span>Contact Us</span></a>
        
        
        
    </div>
<div class="menu4sub"> </div>
      </nav>-->
    </div>
    





     
      </div>
    
<!--
<div class="navbar navbar-fixed-top" data-activeslide="1">
		<div class="container-fluid">
		
			 .navbar-toggle is used as the toggle for collapsed navbar content 
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><img class="logoo" src="<?php $logo=base_url().'resources/images/4.png'; echo $logo ;?>"  /></a>
                          </div>
			
			<div class="nav-collapse collapse navbar-responsive-collapse">
				<ul class="nav row">
                                    
					<li data-slide="1" class="col-12 col-sm-1"><a id="menu-link-1" href="" title="Next Section"><span class="fa fa-home "></span> <span class="text">HOME</span></a></li>
					<li data-slide="2" class="col-12 col-sm-1"><a id="menu-link-2" href="" title="Next Section"><span class="icon icon-user"></span> <span class="text">ABOUT US</span></a></li>
					<li data-slide="3" class="col-12 col-sm-1"><a id="menu-link-3" href="" title="Next Section"><span class="icon icon-briefcase"></span> <span class="text">PORTFOLIO</span></a></li>
					<li data-slide="4" class="col-12 col-sm-1"><a id="menu-link-4" href="#slide-4" title="Next Section"><span class="icon icon-gears"></span> <span class="text">PROCESS</span></a></li>
					<li data-slide="5" class="col-12 col-sm-1"><a id="menu-link-5" href="#slide-5" title="Next Section"><span class="icon icon-heart"></span> <span class="text">CLIENTS</span></a></li>
					<li data-slide="6" class="col-12 col-sm-1"><a id="menu-link-6" href="#slide-6" title="Next Section"><span class="icon icon-envelope"></span> <span class="text">CONTACT</span></a></li>
				</ul>
				<div class="row">
					<div class="col-sm-2 active-menu"></div>
				</div>
			</div> /.nav-collapse 
		</div> /.container 
	</div>
    
<script>
	$(document).ready(function(e) {
//	   alert('hello');
      var menu = $('.active-menu');    
        var wd=menu.css('width');
        ww=parseInt(wd);
        aa=setInterval(function(){
            if(ww>50){clearInterval(aa);}
         menu.css('width',ww+=1);
    },5);
        
	});
	</script>   -->
<nav class="navbar navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#">
        </a>
            <a class="navbar-brand" href="index.html"><img class="logoo" src="<?php $logo=base_url().'resources/images/4.png'; echo $logo ;?>"  /></a>
        
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="asd">
          <!--<li> <a href="<?php  //$home_url=base_url().'main/page';echo $home_url;?>" ><span>Home</span></a></li>-->
          <?php foreach($items as $item){ ?>
             <li> <a href="<?php $url=base_url().'main/page/'.$item['url'];echo $url;?>" ><span><?php echo $item['name'];?></span></a></li>
          <?php }
          if($this->session->userdata('role')=='admin'){?>
            <li> <a href="<?php $url=base_url().'main/admin_board'; echo $url;?>" ><span>Admin Dashboard</span></a></li>
          <?php }?>   
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>