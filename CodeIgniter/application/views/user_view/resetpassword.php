        <div class="container-fluid">	
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <!--<form class="form-horizontal">-->
            <?php
               
                echo validation_errors();
                echo form_open('main/resetpassword/'.$code,array("class"=>"form-horizontal"));?>
            <fieldset>

            <!-- Form Name -->
            <legend>Reset Password</legend>
            <?php
                echo form_label("Password: ","password",array("class"=>"col-md-4 control-label"));
                    $data=array(
                          "type"=>"password",
                          "name"=>"password",
                          "id"=>"password",
                          "placeholder"=>"Enter Password Here",
                          "required"=>"required",
                          "class"=>"form-control input-md"
                    );
                    echo form_input($data);
                    echo '<br/>';
                    echo form_label("ConfirmPassword: ","confirmpassword",array("class"=>"col-md-4 control-label"));
                    $data=array(
                          "type"=>"password",
                          "name"=>"confirmpassword",
                          "id"=>"confirmpassword",
                          "placeholder"=>"Enter Password Here",
                          "required"=>"required",
                          "class"=>"form-control input-md"
                    );
                    echo form_input($data);
                    echo '<br/>';
                    ?>
                    <div class="form-group">
                    <div class="col-md-5 col-md-offset-4">
                   <?php 
                           echo form_submit("submit","submit");?>
                    </div>
             
            </div>
            
            </fieldset>
                <?php echo form_close();
                    ?>
        </div>
        </div>
    
</div>
    <div class="push" style="margin-top:200px;">
    </div>


