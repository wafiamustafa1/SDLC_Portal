<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Users</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Users Table
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="users_table">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>name</th>
                                            <th>email</th>
                                            <th>image</th>
                                            <th>option</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                        <?php
                            foreach ($users as $user)
                            {
                                echo '<tr class="gradeA">';
                                echo '<td>';
                                echo '</td>';
                                echo '<td>';
                                echo $user['name'];
                                echo '</td>';
                                echo '<td>';
                                echo $user['email'];
                                echo '</td>';
                                echo '<td>';
                                $src=base_url().'uploads/'.$user['image'];?>
                                    <img src="<?php echo $src?>" width="50" height="50"/>
                                <?php
                                echo '</td>';
                                echo '<td>';
                                $href= 'delete_user/'.$user['id'];?>
                                <a class="btn btn-danger" href="<?php echo $href;?>">Delete</a>
                                <?php
                                echo '</td>';
                                echo '</tr>';
                            }
                        ?>
               
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
          
 <script>
        $(document).ready(function() {
            $('#users_table').DataTable({
                    responsive: true
            });
        });
 </script> 
