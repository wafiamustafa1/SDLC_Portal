<br>
<br>
<div class="container-fluid prodesign" >
    <div class="row profile ">
		<div class="col-md-3">
			<div class="profile-sidebar">
		
                <div class="profile-userpic">
                    <?php foreach ($user as $u): ?>
                    <img class="img-responsive" alt="" src="<?php $src=base_url().'uploads/'.$u['image'];echo $src;?>"  /> 
		</div>

		<div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <?php echo $u['name'];?>
                     </div>
                    <div class="profile-usertitle-job">
                        <?php echo $u['job'];?>
                    </div>
                </div>
                    <?php endforeach ?>
				<div class="profile-usermenu">
					<ul class="nav">
						
						<li class="active">
                                                    <a href="#" class="setting">
							<i class="glyphicon glyphicon-user"></i>
							Account Settings </a>
						</li>
						
					</ul>
				</div>
			</div>
		</div>
	<div class="col-md-9">
            <div class="profile-content" >
			   Click Account Setting to Edit your profile ........
                           
            </div>
            <div class="profile-content"  id="hidden" style="padding-left:150px;padding-right: 150px;">
          
                <legend>Edit Your Profile</legend>

               <!-- <form class="form-horizontal">-->
               <?php
                echo '<font color="red">'.validation_errors().'</font>';
                echo form_open_multipart("main/edit_user", array('class' => 'form-horizontal'));
               ?>
            <fieldset>
              <?php
                foreach ($user as $u)
                {
              ?>  
            
            <div class="form-group">
                <?php  echo form_label("Edit Your Name: ","name",array('class' => 'control-label','for'=>'name')); ?>
              <!--<label class=" control-label" for="uname">Edit Your Name</label>-->
              <div >
                  <?php
                    $data=array(
                          "type"=>"text",
                          "name"=>"name",
                          "id"=>"name",
                          "placeholder"=>"Enter Full Name",
                          "value"=>$u['name'],
                          "class"=>"form-control input-md",
                          "required"=>"required",
                    );
                    echo form_input($data);?>
                  
                <!--<input  name="username" placeholder="User name from database" class="form-control input-md"  type="text">-->
                   
              </div>
            </div>

            
            <div class="form-group">
                <?php  echo form_label("Edit Your Job: ","job",$attributes = array('class' => 'control-label','for'=>'job')); ?>
              <!--<label class=" control-label" for="newjob">Edit Your Job </label>-->
              <div >
                <!--<input id="co_new_pass" name="newjob" placeholder="user job from database" class="form-control input-md"  type="text">-->
                  <?php  $data=array(
                          "type"=>"text",
                          "name"=>"job",
                          "id"=>"job",
                          "placeholder"=>"Enter your job",
                          "value"=>$u['job'],
                          "required"=>"required",
                          "class"=>"form-control input-md" 
                    );
                    echo form_input($data);?>
              </div>
            </div>
            
            <div class="form-group">
             <!-- <label class=" control-label" for="newimg">Edit Your Image </label>-->
               <?php  echo form_label("Edit Your Image: ","image",$attributes=array('class'=>'control-label','for'=>'image')); ?>
              <div >
                <!--<input id="newimg" name="newimg"  class="form-control input-md"  type="file">-->
                  <?php
                    $data=array(
                          "type"=>"file",
                          "name"=>"image",
                          "id"=>"image",
                          "class"=>"form-control input-md"
                            );
                   
                    echo form_input($data);  
                ?>

              </div>
            </div>
            

            <div class="form-group">
                <div>
                    <?php
                      }
                      $data=array(
                          "name"=>"submit",
                          "value"=>"submit",
                          "class"=>"button ui-inited",
                          "id"=>"hamada",
                          "size"=>"30",
                            );
                     echo form_submit($data);
                     echo form_close();?>
                    <!--<input type="submit" value="Confirm" required=required" size="30" class="button ui-inited"/> -->
                  </div>
             
            </div>
            
            </fieldset>
       <!-- </form>-->
            
               </div>

		</div>
        
	</div>
</div>

<br>
<br>     