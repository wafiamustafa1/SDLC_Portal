<div id="page-wrapper">        
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">  
                <div class="panel-heading">
                    Add Package
                </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <table class="table">
                            <?php
				echo form_open('main/add_package');?>
                                
                                <tr>
                                    <td>
                                        <?php
                                            echo form_label('Packages Name: ', 'name');?>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                            <?php
                                                $data=array(
                                                    "type"=>"text",
                                                    "name"=>"package_name",
                                                    "class"=>"form-control",
                                                    "placeholder"=>"Author Name",
                                                    "required"=>"required",
                                                    "size"=>"50",
                                                    );
                                                echo form_input($data);
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                                
					
                                <tr>
                                    <td>
                                        <label >Number of Projects:</label>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                         <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                       <?php 
                                            $data=array(
                                                        "type"=>"text",
                                                        "name"=>"summary",
                                                        "class"=>"form-control",
                                                        "placeholder"=>"News summary",
                                                        "required"=>"required",
                                                        "size"=>"30",

                                                        );
                                            echo form_input($data);?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label >NDetails:</label>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                        <?php
                                            $data=array(
                                                        "type"=>"text",
                                                        "name"=>"details",
                                                        "class"=>"form-control",
                                                        "placeholder"=>"News Details",
                                                        "required"=>"required",
                                                        "size"=>"30",

                                                        );
                                            echo form_input($data);
                                        ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    
                                        <div class="form-group">
                                            
                                            <td>
						<label >PDate:</label>
                                            </td>
                                            <td>
                                                <div class="input-group margin-bottom-sm">
                                                    <span class="input-group-addon"><i class="fa fa-price fa-fw"></i></span>
                                                    <?php
                                                        $data=array(
                                                                    "type"=>"text",
                                                                    "name"=>"date",
                                                                    "class"=>"form-control",
                                                                    "placeholder"=>"Package Price",
                                                                    "required"=>"required",
                                                                    "size"=>"30",

                                                                    );
                                                        echo form_input($data);
                                                    ?>
                                                </div>
                                            </td>
                                           
                                        </div>
                                    
                                    </td>
                                    
                                </tr>
                                
                                
                                <tr>
                                    <td>
                                       <?php $data=array(
                                                "type"=>"submit",
                                                "name"=>"commit",
                                                "class"=>"btn btn-success",
                                                "value"=>"Save",
                                                "size"=>"35",
                                                );
                                           echo form_submit($data);
                                           echo form_close();?>

                                    </td>
                                </tr>
                        </table>
                        
                        </div>
                       </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
</div>
