<div id="page-wrapper">        
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">  
                <div class="panel-heading">
                    List Packages
                </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive"> 
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="active">
                                        <th>ID</th>
                                        <th>Pacakge Name</th>
                                        <th>Number of Projects</th>
                                        <th>Number of Users</th>
                                        <th>Action</th>
                                    </tr>  
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($packages as $package)
                                        {
                                            $href1=base_url().'main/edit_package/'.$package['id'];
                                            $href2=  base_url().'main/delete_package/'.$package['id'];
                                            echo '<tr>';
                                                echo '<td>';
                                                echo '</td>';
                                                echo '<td>';
                                                echo $package['name'];
                                                echo '</td>';
                                                echo '<td>';
                                                echo $package['project_num'];
                                                echo '</td>';
                                                echo '<td>';
                                                echo $package['users_num'];
                                                echo '</td>';?>
                                                <td>
                                                <i class="fa fa-pencil-square-o fa-fw"></i>
                                                <a href="<?php echo $href1;?>"> Edit</a>
                                                <a href="<?php echo $href2;?>" class="btn btn-danger">
                                                <i class="fa fa-trash-o fa-lg"></i> Delete</a>
                                                </td>;
                                            </tr>
                                     <?php  
                                        }
                                     ?>
                                   
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>