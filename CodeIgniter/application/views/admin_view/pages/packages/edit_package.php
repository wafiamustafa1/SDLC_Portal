 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ckeditor/ckeditor.js"></script>

<div id="page-wrapper">        
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">  
                <div class="panel-heading">
                    Add Package
                </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table">
                                <?php
                                  foreach ($packages as $package)
                                  {
                                    echo form_open('main/edit_package/'.$package['id']);
                                ?>
                                <tr>
                                    <td>
                                        <?php echo form_label('Pakage Name:','name')?>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                            <?php
                                                $data=array(
                                                    "type"=>"text",
                                                    "name"=>"package_name",
                                                    "class"=>"form-control",
                                                    "placeholder"=>"Package Name",
                                                    "required"=>"required",
                                                    "value"=>$package['name'],
                                                    "size"=>"50",
                                                    );
                                                echo form_input($data);
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo form_label('Packages Tool: ', 'package_tool');?>
                                    </td>
                                    <td>
					<div class="control-group">
										
                                            <div class="controls">
						 <?php  
                                                foreach($tools as $tool)
                                                    {
                                                        $data=array(
                                                                    'name'=>'check[]',
                                                                    'value'=>$tool['id'],
                                                                  );
                                                        echo form_checkbox($data);
                                                        echo $tool['name'];
                                                    }
                                                ?>
                                            </div>
					</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label >Number of Projects:</label>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                         <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                         <?php 
                                            $data=array(
                                                        "type"=>"text",
                                                        "name"=>"projects_num",
                                                        "class"=>"form-control",
                                                        "placeholder"=>"Projects Number",
                                                        "required"=>"required",
                                                        "value"=>$package['project_num'],
                                                        "size"=>"30",

                                                        );
                                            echo form_input($data);?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label >Number of Users:</label>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                        <?php
                                            $data=array(
                                                        "type"=>"text",
                                                        "name"=>"users_num",
                                                        "class"=>"form-control",
                                                        "placeholder"=>"Users Number",
                                                        "required"=>"required",
                                                        "value"=>$package['users_num'],
                                                        "size"=>"30",

                                                        );
                                            echo form_input($data);
                                        ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    
                                        <div class="form-group">
                                            
                                            <td>
						<label >Price:</label>
                                            </td>
                                            <td>
                                                <div class="input-group margin-bottom-sm">
                                                    <span class="input-group-addon"><i class="fa fa-price fa-fw"></i></span>
                                                    <?php
                                                        $data=array(
                                                                    "type"=>"text",
                                                                    "name"=>"price",
                                                                    "class"=>"form-control",
                                                                    "placeholder"=>"Package Price",
                                                                    "required"=>"required",
                                                                    "value"=>$package['price'],
                                                                    "size"=>"30",

                                                                    );
                                                        echo form_input($data);
                                                    ?>
                                                </div>
                                            </td>
                                           
                                        </div>
                                    
                                    </td>
                                    
                                </tr>
                                
                                
                                <tr>
                                    <td>
                                       <?php $data=array(
                                                "type"=>"submit",
                                                "name"=>"commit",
                                                "class"=>"btn btn-success",
                                                "value"=>"Edit",
                                                "size"=>"35",
                                                );
                                           echo form_submit($data);
                                  echo form_close();}?>

                                    </td>
                                </tr>
                        </table>
                        </div>
            </div>
        </div>
    </div>
</div>
  <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ckeditor/ckeditor.js"></script>
