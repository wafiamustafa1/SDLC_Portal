        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Menus</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Social Items Table
                        </div>
                        <!-- /.panel-heading -->
                       <?php $this->session->flashdata('error_message')?>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="users_table">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>name</th>
                                            <th>page_url</th>
                                            <th>Status</th>
                                            <th>option</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($social as $item)
                                            {
                                                echo '<tr class="gradeA">';
                                                echo '<td>';
                                                echo '</td>';
                                                echo '<td>';
                                                echo $item['name'];
                                                echo '</td>';
                                                echo '<td>';
                                                echo $item['url'];
                                                echo '</td>';
                                                echo '<td>';
                                                echo $item['status'];
                                                echo '</td>';
                                                echo '<td>';
                                                $href= base_url().'main/edit_social/'.$item['id'];
                                              ?>
                                                <a href="<?php echo $href;?>" class="btn btn-danger">Edit</a>
                                                <?php
                                                echo '</td>';
                                                echo '</tr>';
                                            }
                                    ?>
             </table>
        </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
