<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Menus</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Pages Table
                        </div>
                        <!-- /.panel-heading -->
                       <?php //$this->session->flashdata('error_message')?>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">

                                <?php
                                    echo validation_errors();
                                    foreach($items as $item)
                                     {
                                      echo form_open("main/edit_menu_item/".$item['id']);
                                        echo form_label("Position: ","type");
                                        $options=array("Header","Footer");
                                        if($item['type']=="header")
                                        {
                                            $selected_value=0;
                                        }
                                        else
                                        {
                                            $selected_value=1;
                                        }
                                        echo form_dropdown("position", $options,$selected_value);
                                     }
                                        echo '<br/>';
                                        echo form_submit("submit","submit");
                                    echo form_close();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>