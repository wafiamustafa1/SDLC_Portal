        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Menus</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Pages Table
                        </div>
                        <!-- /.panel-heading -->
                       <?php $this->session->flashdata('error_message')?>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="users_table">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>name</th>
                                            <th>page_url</th>
                                            <th>option</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($items as $item)
                                            {
                                                echo '<tr class="gradeA">';
                                                echo '<td>';
                                                echo '</td>';
                                                echo '<td>';
                                                echo $item['name'];
                                                echo '</td>';
                                                echo '<td>';
                                                echo $item['url'];
                                                echo '</td>';
                                                echo '<td>';
                                                $href= base_url().'main/edit_menu_item/'.$item['id'];
                                                $href1=base_url().'main/delete_menu_item/'.$item['id'];
                                                ?>
                                    <!--<a href="<?php //echo $href;?>" class="btn btn-default">Edit</a>-->
                                               <?php
                                                    if($item['name']== 'home' || $item['name']== 'contact')
                                                    {
                                                        
                                                    }
                                                    else 
                                                    {?>
                                                      <a href="<?php echo $href1;?>" class="btn btn-danger">Delete</a>

                                                   <?php }
                                                echo '</td>';
                                                echo '</tr>';
                                                 }
                                            ?>
             </table>
        </div>
                        </div>
                    </div>
                </div>
            </div>
</div>