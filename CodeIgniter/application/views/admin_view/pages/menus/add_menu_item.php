<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Menus</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Item Form
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php
                                echo validation_errors();
                                echo form_open("main/add_menu_item");
                                    echo form_label("Name: ","name");
                                        $data=array("type"=>"text",
                                                    "name"=>"name",
                                                    "id"=>"name",
                                                    "placeholder"=>"Enter item Name",
                                                    "required"=>"required",
                                                    );
                                    echo form_input($data);
                                    echo '<br/>';
                                    echo form_label("URL: ","url");
                                    $options=array();
                                    foreach($pages as $page)
                                    {
                                        $options[$page['name']]=$page['name'];
                                    }
                                    echo form_dropdown("url", $options);
                                     echo '<br/>';
                                    echo form_label("Position: ","type");
                                    $options=array("Header");
                                    echo form_dropdown("position", $options);
                                    echo '<br/>';
                                    echo form_submit("submit","submit");
                                echo form_close();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
</div>

