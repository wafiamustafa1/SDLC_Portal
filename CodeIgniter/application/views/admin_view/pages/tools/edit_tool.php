
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Tool</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Edit Tool
                        </div>
                        <!-- /.panel-heading -->
                       <?php
        echo validation_errors();
        foreach($tools as $tool)
         {
            echo form_open("main/edit_tool/".$tool['id']);
            echo form_label("Name: ","name");
                $data=array("name"=>"name",
                            "id"=>"name",
                            "value"=>$tool['name'],
                            
                            );
            echo form_input($data);
            echo '<br/>';
             echo form_label("Description: ","description");
                $data=array("name"=>"description",
                            "id"=>"description",
                            "value"=>$tool['description'],
                            
                            );
            echo form_textarea($data);
     
            echo '<br/>';
            
            echo form_label("Price: ","price");
                $data=array("name"=>"price",
                            "id"=>"price",
                            "value"=>$tool['price'],
                            
                            );
            echo form_input($data);
           
            echo '<br/>';
            
            echo form_submit("submit","submit");
            echo form_close();
         }
            
    ?>
                            </div>
                        </div>
                    </div>
                </div>
           
