<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pages</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Pages Table
                        </div>
                        <!-- /.panel-heading -->
                       <?php $this->session->flashdata('error_message')?>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="users_table">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>name</th>
                                            <th>content</th>
                                            <th>option</th>
                                        </tr>
                                    </thead>
                                    <tbody> 

                <?php
                    foreach ($pages as $page)
                    {
                        echo '<tr class="gradeA">';
                        echo '<td>';
                        echo '</td>';
                        echo '<td>';
                        echo $page['name'];
                        echo '</td>';
                        echo '<td>';
                        echo $page['details'];
                        echo '</td>';
                        echo '<td>';
                        $href= 'edit_page/'.$page['id'];
                        $href1='delete_page/'.$page['id'];
                        ?>
                        <a href="<?php echo $href;?>" class="btn btn-default">Edit</a>
                        
                        <a href="<?php echo $href1;?>" class="btn btn-danger">Delete</a>
                        <?php
                        echo '</td>';
                        echo '</tr>';
                    }
                ?>
            </table>
        </div>
                        </div>
                    </div>
                </div>
            </div>
</div>