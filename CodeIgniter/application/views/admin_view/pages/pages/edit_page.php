 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ckeditor/ckeditor.js"></script>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pages</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit Page
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                        <?php
                            echo validation_errors();
                            foreach($pages as $page)
                             {
                                echo form_open("main/edit_page/".$page['id']);
                                echo form_label("Details: ","details");
                                    $data=array("name"=>"details",
                                                "id"=>"details",
                                                "value"=>$page['details'],
                                                "required"=>"required",
                                                );
                                echo form_textarea($data);
                                //echo display_ckeditor($ckeditor);
                                echo '<br/>';
                                echo form_submit("submit","submit");
                            echo form_close();
                            }

                        ?>
</div>
                    </div>
                </div>
            </div>
</div>