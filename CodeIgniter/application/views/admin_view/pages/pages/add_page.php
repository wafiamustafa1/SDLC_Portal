 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ckeditor/ckeditor.js"></script>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pages</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Page
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                    <?php
                        echo validation_errors();
                        echo form_open_multipart("main/add_page");
                            echo form_label("Name: ","name");
                                $data=array("type"=>"text",
                                            "name"=>"name",
                                            "id"=>"name",
                                            "placeholder"=>"Enter item Name",
                                            "required"=>"required",
                                            );
                            echo form_input($data);
                            echo '<br/>';
                            echo form_label("Details: ","details");
                                $data=array(
                                            "name"=>"details",
                                            "id"=>"details",
                                            "required"=>"required",
                                            );
                            echo form_textarea($data);
                            echo display_ckeditor($ckeditor);
                            echo '<br/>';
                            echo form_submit("submit","submit");
                        echo form_close();
                    ?>
                        </div>
                    </div>
                </div>
            </div>
</div>


 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ckeditor/ckeditor.js"></script>
