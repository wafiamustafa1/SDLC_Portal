<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!--<link rel="stylesheet" href="/resources/demos/style.css">-->

  <script>
  $(function() {
    $( "#datepicker" ).datepicker({
      showOn: "button",
      buttonImage: "<?php $logos=base_url().'resources/images/calendar.gif'; echo $logos ;?>",
      buttonImageOnly: true,
      buttonText: "Select date"
    });
  });
  </script>
<div id="page-wrapper">        
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">  
                <div class="panel-heading">
                    Add News
                </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <table class="table">
                            <?php
				echo form_open('main/add_news');?>
                                
                                <tr>
                                    <td>
                                        <?php
                                            echo form_label('Author: ', 'author');?>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                            <?php
                                                $data=array(
                                                    "type"=>"text",
                                                    "name"=>"author",
                                                    "class"=>"form-control",
                                                    "placeholder"=>"Author Name",
                                                    "required"=>"required",
                                                    "size"=>"50",
                                                   
                                                    );
                                                echo form_input($data);
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td>
                                        <label >Summary:</label>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                         <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                       <?php 
                                            $data=array(
                                                        "type"=>"text",
                                                        "name"=>"summery",
                                                        "class"=>"form-control",
                                                        "placeholder"=>"News Summary",
                                                        "required"=>"required",
                                                        "size"=>"30",


                                                        );
                                            echo form_input($data);?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label >Details:</label>
                                    </td>
                                    <td>
                                        <div class="input-group margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                        <?php
                                            echo '<br/>';
                                            $data=array(
                                                    "name"=>"details",
                                                    "id"=>"details",
                                                   "required"=>"required",
                                                );
                                            echo form_textarea($data);
                                            echo display_ckeditor($ckeditor);
                                        ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    
                                        <div class="form-group">
                                            
                                            <td>
						<label >date:</label>
                                            </td>
                                            <td>
                                                <div class="input-group margin-bottom-sm">
                                                    <span class="input-group-addon"><i class="fa fa-price fa-fw"></i></span>
                                                    <?php
                                                        $data=array(
                                                                    "type"=>"text",
                                                                    "name"=>"date",
                                                                    "class"=>"search_event",
                                                                    "placeholder"=>"News Date",
                                                                    "required"=>"required",
                                                                    "size"=>"30",
                                                                     "id"=>"datepicker",


                                                                    );
                                                        echo form_input($data);
                                                    ?>
                                                </div>
                                            </td>
                                           
                                        </div>
                                    
                                    </td>
                                    
                                </tr>
                                
                                
                                <tr>
                                    <td>
                                       <?php $data=array(
                                                "type"=>"submit",
                                                "name"=>"commit",
                                                "class"=>"btn btn-success",
                                                "value"=>"Save",
                                                "size"=>"35",
                                                );
                                           echo form_submit($data);
                                           echo form_close();?>

                                    </td>
                                </tr>
                        </table>
                        
                        </div>
                       </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
</div>
