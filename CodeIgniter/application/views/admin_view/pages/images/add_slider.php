<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Images</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Image
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <?php
                                echo validation_errors();
                                echo $error;
                                echo form_open_multipart("main/add_image");
                                echo form_label("Image: ","image");
                                    $data=array(
                                          "type"=>"file",
                                          "name"=>"image",
                                          "id"=>"image",
                                    );

                                    echo form_input($data);
                                    echo '<br/>';
                                     echo form_label("Details: ","image");
                                    $data=array(
                                          "type"=>"text",
                                          "name"=>"details",
                                          "id"=>"details",
                                    );

                                    echo form_input($data);
                                    echo '<br/>';         
                                    echo form_submit("submit","submit");
                                echo form_close();
                               ?>
                        </div>
                    </div>
                </div>
            </div>
</div>
