
<br>

<div class="container-fluid">
		
 <div class="row">
 		<div class="col-md-10 col-md-offset-1">
            <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">

                <ol class="carousel-indicators">
                    <?php $i=0; foreach($images as $image){?>
                    
                     <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>"
                         class="<?php if($i==0){echo 'active';}else{echo '';}?>">
                     </li>
                    <?php $i++; }
                    ?> 
                </ol>
                
                <div class="carousel-inner">
                    <?php $i=0; foreach ($images as $image){?>  
                    <div class="<?php if($i==0){
                                        echo 'active item';}else{echo 'item';}?>">  
                        <img src="<?php $src=base_url().'uploads/'.$image['image']; echo $src;?>"/>
                        <div class="carousel-caption">
                          <h3><?php echo $image['details']?></h3>
                          
                        </div>
                    </div>
                    <?php $i++;}?>
                    
                </div>

                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
     </div>   
		<div class="row">
			<div  style="padding-top:20px">
				<div class="col-md-8 col-md-offset-2 col-xs-12 search_panel">
					 <form role="search " >
							<i class="fa fa-search fa-2x "></i>


							<input type="text"  size="50" class="" placeholder="Search">

							<select>
								<option>Services </option>
								<option>Offers </option>
								<option>packages </option>
								<option> </option>
							</select>

							<button  type="submit" class=" btn btn-default">Search</button>
					 </form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-1 text-left">
				<div class="news_head">
					<h2>News</h2>
				</div>
                            <p class="h4"><b><a> Lorem ipsum dolor sit amet</a></b><br /></p>
				<p class="text-left prag_news"> Lorem ipsum dolor sit amet,
					consectetur adipiscing elit. Nullam neque urna, cursus ac euismod nec, rhoncus sit amet ipsum.
					Suspendisse consequat scelerisque arcu, vel sollicitudin nibh.
				</p>
				<div align="right">
					<div class="read_more ">
						<a class="btn-xs" href="#">Read More</a>
					</div>
				</div>
				<p class="h4"><b> <a> Lorem ipsum dolor sit amet</a></b><br /></p>
				<p class="text-left prag_news"> Lorem ipsum dolor sit amet,
					consectetur adipiscing elit. Nullam neque urna, cursus ac euismod nec, rhoncus sit amet ipsum.
					Suspendisse consequat scelerisque arcu, vel sollicitudin nibh.
				</p>
				<div align="right">
					<div class="read_more ">
						<a class="btn-xs" href="#">Read More</a>
					</div>
				</div>
				<p class="h4 header_4"><b> <a> Lorem ipsum dolor sit amet</a></b><br /></p>
				<p class="text-left prag_news"> Lorem ipsum dolor sit amet,
						consectetur adipiscing elit. Nullam neque urna, cursus ac euismod nec, rhoncus sit amet ipsum.
						Suspendisse consequat scelerisque arcu, vel sollicitudin nibh
				</p>
				<div align="right">
					<div class="read_more ">
						<a class="btn-xs" href="#">Read More</a>
					</div>
				</div>
			   <a href="#">See More News</a>
			</div>
			<div class=" col-md-3 col-md-offset-1 ">
				<div class="news_head">
					<h2>Events</h2>
				</div>
                            <input type="text" id="datepicker" class="search_event">
				<p class="h4 header_4"><b>today Events</b><br /></p>
				<p class="h4 header_4"><b><a> Lorem ipsum dolor sit amet</a></b><br /></p>
				<p class="text-left prag_news"  style="color:gray;"> Lorem ipsum dolor sit amet,
					consectetur adipiscing elit. Nullam neque urna,
				</p>
				<div align="right">
					<div class="read_more ">
						<a class="btn-xs" href="#">Read More</a>
					</div>
				</div>
				<p class="h4 header_4"><b><a> Lorem ipsum dolor sit amet</a></b><br /></p>
				<p class="text-left prag_news"> Lorem ipsum dolor sit amet,
					   consectetur adipiscing elit. Nullam neque urna,
				</p>
				<div align="right">
					<div class="read_more ">
						<a class="btn-xs" href="#">Read More</a>
					</div>
				</div>
				<a href="#">See More Events</a>
			</div>
		</div>
		<div id="price" class="pricing ">
					<div class="container ">
                                            <div class="row">
                                                <div class="price col-xs-12 col-md-12">
							<h2>Pricing</h2>
							<p>Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero mal suada
								<span>feugiat. Curabitur aliquet quam uis lorem</span></p>
						</div>
                                            </div>
                                            </div>
                                </div>
    
                                            <div class="row col-md-11 col-md-offset-1" >
                                              <?php foreach ($packages as $package){?>
						<div class="basic col-md-3">
							<div class="business">
                                                            <h2><?php echo $package['name'];?></h2>
								<p>Best for small business</p>
							</div>
							<div class="value">
								<p><?php echo $package['price']?>$</p>
							</div>
							<ul>
                                                            <li><span><?php echo $package['project_num']; ?></span> Projects</li>
								<li><span><?php echo $package['users_num']; ?></span> Users</li>
								
								<li class="gd"><span>24h</span> Ready</li>
							</ul>
							<div class="buy-me">
								<a href="#">Buy Me</a>
							</div>
						</div>
                                              <?php }?>
                                   
                                         </div>
                                               
                                                
                                                
                                                
                                                
						

 
